# TRADE ME
TradeMe est une plateforme pour trouver des ouvriers qualifiés (électricien, maçon, plombier, charpentier, etc) pour intervenir sur des projets de construction.
- Des prestataires (contractor) emploient des ouvriers qualifiés (tradesman) sur quelques jours ou quelques semaines pour des tâches spécifiques
- Les ouvriers peuvent venir et partir sur un même projet

## Use cases implémentés
- Ajout d'un utilisateur (add member)
- Abonnement mensuel (monthly payment)
- Création d'un projet (create project)
- Demande d'ouvriers (request tradesman)
- Interrompre les interventions d'un ouvrier (terminate tradesman)
- Clôture d'un projet (close project)

## Choix architecturaux
Nous avons réalisé une séparation de l'applicatif en différentes couches :
 - exposition
 - application
 - domain
 - infrastructure

L'applicatif est également organisé autour des différents use cases, chacun reprenant les couches ci-dessus.<br>
Nous avons décidé d'utiliser du CQS (Command Query Separation) pour dissocier les simples requêtes des commandes pouvant modifier nos données.<br>
Voici un schéma de principe du command/query bus utilisé.
```mermaid
flowchart LR
    subgraph Exposition
        A
    end
    A[Controller] --Command/Query Bus--> B[Command/Query Handler]
    subgraph Application/Domain
    direction LR
        B
    end    
    B --> C[Repository]
    subgraph Infrastructure
        C
    end
```

De la même manière, nous avons implémenté des évènements applicatifs, gérés par un event dispatcher.<br>
Le même Event Dispatcher se charge de l'envoi de l'ensemble des évènements à leurs abonnés (listeners) respectifs.
```mermaid
flowchart LR
    A[Event Dispatcher] --Event--> B[Listener]
    A --Event--> C[Another listener]
```

## Détails des use cases
### Ajout d'un utilisateur (add member)
L'ajout d'un utilisateur suit un canal de souscription.<br>
Dans un premier temps, l'utilisateur est créé et ne contient que son nom et prénom.
```mermaid
flowchart LR
    subgraph 1. user creation
    direction LR
        A[firstname, lastname] --> B[user created]
    end
```
Dans un second temps, le profil de l'utilisateur est complété :
- ajout ou modification de son mot de passe
- ajout ou modification de sa carte de crédit
- affectation à une catégorie : prestataire ou ouvrier qualifié
- choix du métier et d'un tarif journalier s'il est un ouvrier qualifié

Toutes ces actions peuvent être réalisées dans n'importe quel ordre.
```mermaid
flowchart LR
    subgraph 2. user update
    direction LR
        D[password] --> C[user]
        E[credit card] --> C[user]
        F[category] --> C[user]
        G[job, daily rate] --> C[user]
    end
```
Enfin, on peut procéder à la validation de l'utilisateur.<br>
Pour cela il est vérifié si son profil est complet, c'est-à-dire si toutes les actions de l'étape précédente (2. User update) ont été réalisées.<br>
Dans le cas où son profil est valide, le paiement initial est réalisé et son profil est alors renseigné comme valide.
```mermaid
flowchart LR
    subgraph 3. user application
    direction LR
        H[approve user] --> I{application valid ?}
        I -- Yes --> J[Initial payment]
        I -- No --> InvalidUserApplicationException
        J --> Register
        Register --> K[valid user]
    end
```
### Abonnement mensuel (monthly payment)
Chaque utilisateur valide (ayant payé le paiement initial) est soumis à un abonnement mensuel.
- Une tâche planifiée est exécutée à intervalle régulier (ici 1 fois par jour).
- Cette tâche (Monthly payment) réalise dans un premier temps une requête pour récupérer les utilisateurs valides.
- Pour chacun, si son dernier paiement remonte à plus d'un mois, une commande est envoyée pour que cet utilisateur paye son abonnement.

```mermaid
flowchart LR
    direction LR
        A[Task Scheduler] --> B[Monthly payment]
        B --QueryBus--> C[1. Retrieve Valid Users]
        B --CommandBus--> D[2. Process Regular Payment]
```

### Création d'un projet (create project)
De la même manière que pour la création d'un utilisateur, la création d'un projet est réalisée en plusieurs étapes successives.<br>
Dans un premier temps, le projet est créé avec pour seules informations son titre, la ville où il aura lieu et le prestataire qui le pilotera.
```mermaid
flowchart LR
    subgraph 1. project creation
    direction LR
        A[title, location, contractor] --> B[project created]
    end
```

Ensuite ce projet peut être complété avec une période et des besoins.<br>
Une période est définie avec une date de début et une date de fin.<br>
Un besoin est défini de la façon suivante :
- un métier recherché
- le taux journalier proposé pour les ouvriers de ce métier
- le nombre d'ouvriers nécessaire pour ce métier
```mermaid
flowchart LR
    subgraph 2. project update
    direction LR
        A[period, needs] --> B[project updated]
    end
```

Enfin le projet peut être activé s'il est complet.
```mermaid
flowchart LR
    subgraph 3. project activation
    direction LR
        A[activate project] --> B{project valid ?}
        B -- Yes --> C[activate]
        B -- No --> InvalidProjectActivationException
        C --> D[active project]

    end
```
### Demande d'ouvriers (request tradesman)
Il est possible de réaliser une demande d'ouvriers sur un projet actif.
L'ensemble des besoins est alors étudié et confronté à la liste des ouvriers valides.
Pour chaque besoin, il est vérifié :
- que le métier demandé est bien celui de l'ouvrier
- que le tarif proposé est au moins égal aux prétentions de l'ouvrier
- que l'ouvrier est toujours disponible pour la période souhaitée

La disponibilité d'un ouvrier est obtenue par lecture de ses interventions programmées.
Si la moindre intersection est rencontrée entre une intervention et la période demandée, alors l'ouvrier n'est pas disponible.

Les ouvriers sont ensuite sélectionnés automatiquement par prétention croissante parmi la liste restante.
Les ouvriers ne seront affectés au projet que si l'ensemble des besoins en ouvriers est satisfait. Le cas contraire, aucun ouvrier n'est affecté.

```mermaid
flowchart TB
    A[request tradesman] --> B{active project ?}
    B -- Yes --> C[match tradesman]
    B -- No --> InactiveProjectException
    C --> D[best fit tradesmen]
    D --> E{all needs fulfilled ?}
    E -- Yes --> F[tradesmen assigned to project]
    E -- No --> NoMatchingTradesmanException
```

### Interrompre les interventions d'un ouvrier (terminate tradesman)
Un ouvrier peut être libéré de ses différentes interventions.
```mermaid
flowchart LR
    A[terminate tradesman] --> B[search for active projects]
    B --> C[clear interventions]
    C --> D[tradesman available]
```

### Clôture d'un projet (close project)
À la cloture d'un projet, ses ouvriers sont détachés de ce dernier.
```mermaid
flowchart LR
    A[close project] --> B[release tradesmen]
    B --> C[clear interventions]
    C --> D[project closed]
```