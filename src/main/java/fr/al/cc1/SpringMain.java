package fr.al.cc1;

import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.kernel.domain.Job;
import fr.al.cc1.use_cases.project.exposition.*;
import fr.al.cc1.use_cases.user.kernel.domain.CreditCard;
import fr.al.cc1.use_cases.user.kernel.exposition.*;
import fr.al.cc1.use_cases.user.kernel.infrastructure.TaskScheduler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class SpringMain {
    public static void main(String[] args) {
        final ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringMain.class, args);

        //Create tradesman
        UserController userController = applicationContext.getBean(UserController.class);

        //1.Add user
        final CreateUserDTO createUserDTO = new CreateUserDTO("Fleuret", "Nathan");
        final Id id = userController.add(createUserDTO);

        //2.Add password for user
        final UpdateUserPasswordDTO updateUserPasswordDTO = new UpdateUserPasswordDTO(id, "pass");
        userController.updatePassword(updateUserPasswordDTO);

        //3.Add credit card for user
        final CreditCard card = new CreditCard("1234123412341234");
        final UpdateUserCreditCardDTO updateUserCreditCard = new UpdateUserCreditCardDTO(id, card);
        userController.updateCreditCard(updateUserCreditCard);

        //4.Define user category
        final UpdateUserCategoryDTO updateUserCategoryDTO = new UpdateUserCategoryDTO(id, "tradesman");
        userController.updateUserCategory(updateUserCategoryDTO);

        //5.Set job
        final SetTradesmanJobDTO setTradesmanJobDTO = new SetTradesmanJobDTO(id, Job.ELECTRICIAN.ordinal(), 150);
        userController.setTradesmanJob(setTradesmanJobDTO);

        //6.Process initial payment for user
        userController.approveUser(id);

        // --- Create contractor
        //1.Add user
        final CreateUserDTO contractor = new CreateUserDTO("Oulmas", "Meziane");
        final Id id2 = userController.add(contractor);

        //2.Add password for user
        final UpdateUserPasswordDTO updateUserPasswordDTO2 = new UpdateUserPasswordDTO(id2, "pass");
        userController.updatePassword(updateUserPasswordDTO2);

        //3.Add credit card for user
        final CreditCard card2 = new CreditCard("1234123412341234");
        final UpdateUserCreditCardDTO updateUserCreditCard2 = new UpdateUserCreditCardDTO(id2, card);
        userController.updateCreditCard(updateUserCreditCard2);

        //4.Define user category
        final UpdateUserCategoryDTO updateUserCategoryDTO2 = new UpdateUserCategoryDTO(id2, "contractor");
        userController.updateUserCategory(updateUserCategoryDTO2);

        //5.Process initial payment for user
        userController.approveUser(id2);

        // --- Create Project
        //1.Define title and location
        ProjectController projectController = applicationContext.getBean(ProjectController.class);
        final CreateProjectDTO projectDTO = new CreateProjectDTO("cuisine", "Rouen", id2);
        final Id projectId = projectController.createProject(projectDTO);

        //2.Set needs and period
        final ProjectNeedDTO need = new ProjectNeedDTO(Job.ELECTRICIAN.ordinal(), 200.0, 1);
        final UpdateProjectDTO updateProjectDTO = new UpdateProjectDTO(projectId, List.of(need), LocalDateTime.now(), LocalDateTime.now().plusDays(2));
        projectController.updateProject(updateProjectDTO);

        //3.Activate project
        projectController.activateProject(projectId);

        //4.Request Tradesman
        projectController.requestTradesman(projectId);

        // --- TaskScheduler (for monthly payment)
        TaskScheduler taskScheduler = applicationContext.getBean(TaskScheduler.class);
        taskScheduler.start();
    }
}
