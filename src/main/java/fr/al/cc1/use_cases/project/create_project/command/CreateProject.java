package fr.al.cc1.use_cases.project.create_project.command;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;

public final class CreateProject implements Command {
    public final String title;
    public final String city;
    public final Id contractorId;

    public CreateProject(String title, String city, Id contractorId) {
        this.title = title;
        this.city = city;
        this.contractorId = contractorId;
    }
}
