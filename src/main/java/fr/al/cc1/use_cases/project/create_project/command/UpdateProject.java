package fr.al.cc1.use_cases.project.create_project.command;

import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.kernel.domain.ProjectNeed;
import fr.al.cc1.kernel.domain.Command;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public final class UpdateProject implements Command {
    public final Id projectId;
    public final List<ProjectNeed> needs;
    public final LocalDateTime startDate;
    public final LocalDateTime endDate;

    public UpdateProject(Id projectId, List<ProjectNeed> needs, LocalDateTime startDate, LocalDateTime endDate) {
        this.projectId = projectId;
        this.needs = needs;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
