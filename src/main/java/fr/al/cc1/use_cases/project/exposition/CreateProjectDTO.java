package fr.al.cc1.use_cases.project.exposition;

import fr.al.cc1.kernel.domain.Id;

public final class CreateProjectDTO {
    public final String title;
    public final String city;
    public final Id contractorId;

    public CreateProjectDTO(String title, String city, Id contractorId) {
        this.title = title;
        this.city = city;
        this.contractorId = contractorId;
    }


}
