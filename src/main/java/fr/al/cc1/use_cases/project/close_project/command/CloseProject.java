package fr.al.cc1.use_cases.project.close_project.command;

import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Command;

public final class CloseProject implements Command {
    public final Id projectId;

    public CloseProject(Id projectId) {
        this.projectId = projectId;
    }
}
