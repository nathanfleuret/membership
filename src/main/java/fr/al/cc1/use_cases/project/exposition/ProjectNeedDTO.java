package fr.al.cc1.use_cases.project.exposition;

import fr.al.cc1.use_cases.kernel.domain.DailyRate;

public final class ProjectNeedDTO {
    public final int jobValue;
    public final DailyRate dailyRate;
    public final int tradesmanQuantity;

    public ProjectNeedDTO(int jobValue, double dailyRate, int tradesmanQuantity) {
        this.jobValue = jobValue;
        this.dailyRate = new DailyRate(dailyRate);
        this.tradesmanQuantity = tradesmanQuantity;
    }
}
