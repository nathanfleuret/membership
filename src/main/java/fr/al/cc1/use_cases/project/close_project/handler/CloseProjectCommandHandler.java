package fr.al.cc1.use_cases.project.close_project.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.Event;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Project;
import fr.al.cc1.use_cases.project.close_project.ProjectClosedEvent;
import fr.al.cc1.use_cases.project.close_project.command.CloseProject;
import fr.al.cc1.use_cases.user.kernel.domain.Tradesman;
import fr.al.cc1.use_cases.user.kernel.domain.User;

import java.util.List;
import java.util.stream.Collectors;

public final class CloseProjectCommandHandler implements CommandHandler<CloseProject, Void> {

    private final EventDispatcher<Event> eventDispatcher;
    private final Repository<Project> projectRepository;
    private final Repository<User> userRepository;

    public CloseProjectCommandHandler(EventDispatcher<Event> eventDispatcher, Repository<Project> projectRepository, Repository<User> userRepository) {
        this.eventDispatcher = eventDispatcher;
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Void handle(CloseProject command) {
        final Project project = projectRepository.findById(command.projectId);

        for (Tradesman tradesman : project.getAssignedTradesmen()) {
            tradesman.getTradesmanInterventions()
                    .removeIf(intervention -> intervention.getProjectId() == project.getId());
            userRepository.save(tradesman);
        }
        project.getAssignedTradesmen().clear();
        project.deactivate();
        projectRepository.save(project);
        eventDispatcher.dispatch(new ProjectClosedEvent(project));
        return null;
    }
}
