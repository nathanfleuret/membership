package fr.al.cc1.use_cases.project.close_project;

import fr.al.cc1.kernel.ApplicationEvent;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.kernel.domain.Project;

public final class ProjectClosedEvent implements ApplicationEvent {
    public final Project project;

    public ProjectClosedEvent(Project project) {
        this.project = project;
    }
}
