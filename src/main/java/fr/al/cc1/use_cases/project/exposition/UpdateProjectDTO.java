package fr.al.cc1.use_cases.project.exposition;

import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.kernel.domain.ProjectNeed;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public final class UpdateProjectDTO {
    public Id projectId;
    public List<ProjectNeedDTO> needs;
    public LocalDateTime startDate;
    public LocalDateTime endDate;

    public UpdateProjectDTO(Id projectId, List<ProjectNeedDTO> needs, LocalDateTime startDate, LocalDateTime endDate){
          this.projectId = projectId;
          this.needs = needs;
          this.startDate = startDate;
          this.endDate = endDate;
    }

}
