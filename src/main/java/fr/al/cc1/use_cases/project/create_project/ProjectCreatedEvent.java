package fr.al.cc1.use_cases.project.create_project;

import fr.al.cc1.kernel.ApplicationEvent;
import fr.al.cc1.use_cases.kernel.domain.Project;

public final class ProjectCreatedEvent implements ApplicationEvent {
    public final Project project;

    public ProjectCreatedEvent(Project project) {
        this.project = project;
    }
}
