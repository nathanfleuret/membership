package fr.al.cc1.use_cases.project.create_project;

import fr.al.cc1.kernel.EventListener;

public final class ProjectCreatedEventListener implements EventListener<ProjectCreatedEvent> {
    @Override
    public void listenTo(ProjectCreatedEvent event) {
        System.out.println("Project created :" + event.project);
    }
}
