package fr.al.cc1.use_cases.project.close_project;

import fr.al.cc1.kernel.EventListener;

public final class ProjectClosedEventListener implements EventListener<ProjectClosedEvent> {

    @Override
    public void listenTo(ProjectClosedEvent event) {
        System.out.println("project closed : " + event.project);
    }
}
