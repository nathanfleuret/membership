package fr.al.cc1.use_cases.project.create_project.command;

import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Command;

public final class ActivateProject implements Command {
    public final Id projectId;

    public ActivateProject(Id projectId) {
        this.projectId = projectId;
    }
}
