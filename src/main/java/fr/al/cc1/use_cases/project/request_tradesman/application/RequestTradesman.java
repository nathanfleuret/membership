package fr.al.cc1.use_cases.project.request_tradesman.application;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;

public final class RequestTradesman implements Command {

    public final Id projectId;

    public RequestTradesman(Id projectId) {
        this.projectId = projectId;
    }
}
