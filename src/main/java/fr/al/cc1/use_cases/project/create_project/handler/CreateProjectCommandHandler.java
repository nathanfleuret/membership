package fr.al.cc1.use_cases.project.create_project.handler;

import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Address;
import fr.al.cc1.use_cases.kernel.domain.Project;
import fr.al.cc1.use_cases.kernel.domain.ProjectBuilder;
import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.Event;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.use_cases.project.create_project.command.CreateProject;
import fr.al.cc1.use_cases.project.create_project.ProjectCreatedEvent;
import fr.al.cc1.use_cases.user.kernel.domain.Contractor;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class CreateProjectCommandHandler implements CommandHandler<CreateProject,Id> {

    private final EventDispatcher<Event> eventDispatcher;
    private final Repository<Project> projectRepository;
    private final Repository<User> userRepository;

    public CreateProjectCommandHandler(EventDispatcher<Event> eventDispatcher, Repository<Project> projectRepository, Repository<User> userRepository) {
        this.eventDispatcher = eventDispatcher;
        this.projectRepository = projectRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Id handle(CreateProject command) {
        Contractor contractor = (Contractor) userRepository.findById(command.contractorId);
        final Id projectId = projectRepository.nextIdentity();
        Project project = new ProjectBuilder()
                .withId(projectId)
                .withContractorId(projectId)
                .withTitle(command.title)
                .withAddress(new Address(command.city))
                .build();
        projectRepository.save(project);
        eventDispatcher.dispatch(new ProjectCreatedEvent(project));
        return projectId;
    }
}
