package fr.al.cc1.use_cases.project.create_project.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.Event;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Project;
import fr.al.cc1.use_cases.project.create_project.command.ActivateProject;

public final class ActivateProjectCommandHandler implements CommandHandler<ActivateProject,Void> {

    private final EventDispatcher<Event> eventDispatcher;
    private final Repository<Project> projectRepository;

    public ActivateProjectCommandHandler(EventDispatcher<Event> eventDispatcher, Repository<Project> projectRepository) {
        this.eventDispatcher = eventDispatcher;
        this.projectRepository = projectRepository;
    }

    @Override
    public Void handle(ActivateProject command) {
        Project project = projectRepository.findById(command.projectId);
        project.activate();
        this.projectRepository.save(project);
        return null;
    }
}
