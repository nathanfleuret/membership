package fr.al.cc1.use_cases.project.create_project.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.Event;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Project;
import fr.al.cc1.use_cases.kernel.domain.WindowedPeriod;
import fr.al.cc1.use_cases.project.create_project.command.UpdateProject;
import fr.al.cc1.use_cases.project.create_project.domain.AlreadyActiveProjectException;

public final class UpdateProjectCommandHandler implements CommandHandler<UpdateProject, Void> {
    private final EventDispatcher<Event> eventDispatcher;
    private final Repository<Project> projectRepository;

    public UpdateProjectCommandHandler(EventDispatcher<Event> eventDispatcher, Repository<Project> projectRepository) {
        this.eventDispatcher = eventDispatcher;
        this.projectRepository = projectRepository;
    }

    @Override
    public Void handle(UpdateProject command) {
        final Project project = projectRepository.findById(command.projectId);
        if (project.isActive())
            throw new AlreadyActiveProjectException();
        final WindowedPeriod period = new WindowedPeriod(command.startDate, command.endDate);
        project.setPeriod(period);
        project.setNeeds(command.needs);
        this.projectRepository.save(project);
        return null;
    }
}
