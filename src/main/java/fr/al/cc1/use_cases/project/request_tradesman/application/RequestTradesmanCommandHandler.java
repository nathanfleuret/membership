package fr.al.cc1.use_cases.project.request_tradesman.application;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Intervention;
import fr.al.cc1.use_cases.kernel.domain.NoSuchEntityException;
import fr.al.cc1.use_cases.kernel.domain.Project;
import fr.al.cc1.use_cases.kernel.domain.ProjectNeed;
import fr.al.cc1.use_cases.project.create_project.domain.InactiveProjectException;
import fr.al.cc1.use_cases.user.kernel.domain.Tradesman;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import fr.al.cc1.use_cases.project.request_tradesman.domain.NoMatchingTradesmanException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public final class RequestTradesmanCommandHandler implements CommandHandler<RequestTradesman, Void> {
    public final Repository<User> userRepository;
    public final Repository<Project> projectRepository;

    public RequestTradesmanCommandHandler(Repository<User> userRepository, Repository<Project> projectRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Void handle(RequestTradesman command) {
        Project project = this.projectRepository.findById(command.projectId);

        if (!project.isActive())
            throw new InactiveProjectException();

        List<Tradesman> validTradesmen = userRepository.find(User::isValid).stream()
                .filter(user -> user.getClass() == Tradesman.class)
                .map(user -> (Tradesman) user)
                .collect(Collectors.toList());

        for (ProjectNeed need : project.getNeeds()) {
            List<Tradesman> matchTradesmen = validTradesmen.stream()
                    .filter(tradesman -> tradesman.getJob().equals(need.getJob()))
                    .filter(tradesman -> tradesman.isAvailableFor(project.getPeriod()))
                    .filter(tradesman -> tradesman.getDailyRate().compareTo(need.getDailyRate()) <= 0)
                    .sorted(Comparator.comparing(Tradesman::getDailyRate))
                    .collect(Collectors.toList());

            if (matchTradesmen.size() < need.getTradesmanQuantity()) throw new NoMatchingTradesmanException();

            for (int i = 0; i < need.getTradesmanQuantity(); i++) {
                //add tradesmen in project list of assigned tradesmen
                project.getAssignedTradesmen().add(matchTradesmen.get(i));
            }
        }
        //add new intervention + save
        for (Tradesman tradesman : project.getAssignedTradesmen()) {
            tradesman.getTradesmanInterventions()
                    .add(new Intervention(project.getPeriod(), project.getId()));
            userRepository.save(tradesman);
        }
        projectRepository.save(project);
        return null;
    }
}
