package fr.al.cc1.use_cases.project.exposition;

import fr.al.cc1.kernel.CommandBus;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.kernel.domain.Job;
import fr.al.cc1.use_cases.kernel.domain.ProjectNeed;
import fr.al.cc1.use_cases.project.close_project.command.CloseProject;
import fr.al.cc1.use_cases.project.create_project.command.ActivateProject;
import fr.al.cc1.use_cases.project.create_project.command.CreateProject;
import fr.al.cc1.use_cases.project.create_project.command.UpdateProject;
import fr.al.cc1.use_cases.project.request_tradesman.application.RequestTradesman;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public final class ProjectController {
    private final CommandBus commandBus;

    public ProjectController(CommandBus commandBus) {
        this.commandBus = commandBus;
    }

    @PostMapping(value = "/createProject", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    public Id createProject(@RequestBody CreateProjectDTO projectDTO) {
        return this.commandBus.send(new CreateProject(projectDTO.title, projectDTO.city, projectDTO.contractorId));
    }

    @PostMapping(value = "/updateProject", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object updateProject(@RequestBody UpdateProjectDTO projectDTO) {
        List<ProjectNeed> needs = projectDTO.needs.stream().map(projectNeedDTO -> new ProjectNeed(Job.values()[projectNeedDTO.jobValue],
                        projectNeedDTO.dailyRate,
                        projectNeedDTO.tradesmanQuantity))
                .collect(Collectors.toList());
        this.commandBus.send(new UpdateProject(projectDTO.projectId, needs, projectDTO.startDate, projectDTO.endDate));
        return null;
    }

    @PostMapping(value = "/activateProject", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object activateProject(@RequestBody Id projectId) {
        this.commandBus.send(new ActivateProject(projectId));
        return null;
    }

    @PostMapping(value = "/closeProject", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object closeProject(@RequestBody Id projectId) {
        this.commandBus.send(new CloseProject(projectId));
        return null;
    }

    @PostMapping(value = "/requestTradesman", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object requestTradesman(@RequestBody Id projectId) {
        this.commandBus.send(new RequestTradesman(projectId));
        return null;
    }

}

