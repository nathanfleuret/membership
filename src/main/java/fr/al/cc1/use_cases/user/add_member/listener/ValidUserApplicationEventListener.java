package fr.al.cc1.use_cases.user.add_member.listener;

import fr.al.cc1.kernel.EventListener;
import fr.al.cc1.use_cases.user.add_member.event.ValidUserApplicationEvent;

public final class ValidUserApplicationEventListener implements EventListener<ValidUserApplicationEvent> {

    @Override
    public void listenTo(ValidUserApplicationEvent event) {
        System.out.println("valid application for user :" + event.user);
    }
}
