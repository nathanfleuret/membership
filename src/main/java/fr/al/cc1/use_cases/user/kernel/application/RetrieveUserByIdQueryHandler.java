package fr.al.cc1.use_cases.user.kernel.application;

import fr.al.cc1.kernel.QueryHandler;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class RetrieveUserByIdQueryHandler implements QueryHandler<RetrieveUserById, User> {

    private final Repository<User> userRepository;

    public RetrieveUserByIdQueryHandler(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User handle(RetrieveUserById query) {
        return this.userRepository.findById(query.id);
    }
}
