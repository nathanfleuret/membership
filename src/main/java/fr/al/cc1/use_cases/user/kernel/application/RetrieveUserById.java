package fr.al.cc1.use_cases.user.kernel.application;

import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Query;

public final class RetrieveUserById implements Query {
    public final Id id;

    public RetrieveUserById(Id id) {
        this.id = id;
    }
}
