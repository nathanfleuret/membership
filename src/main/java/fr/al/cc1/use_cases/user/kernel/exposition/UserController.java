package fr.al.cc1.use_cases.user.kernel.exposition;

import fr.al.cc1.kernel.CommandBus;
import fr.al.cc1.kernel.QueryBus;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.kernel.domain.DailyRate;
import fr.al.cc1.use_cases.kernel.domain.Job;
import fr.al.cc1.use_cases.user.add_member.command.*;
import fr.al.cc1.use_cases.user.kernel.application.RetrieveUserById;
import fr.al.cc1.use_cases.user.kernel.application.RetrieveUsers;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public final class UserController {
    private final CommandBus commandBus;
    private final QueryBus queryBus;

    public UserController(CommandBus commandBus, QueryBus queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }

    //region READ
    @GetMapping(value = "/all", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<UserDTO> getAll() {
        List<User> users = this.queryBus.send(new RetrieveUsers());
        return users.stream()
                .map(user ->
                        new UserDTO(user.getId(), user.getLastname(), user.getFirstname()))
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserDTO getById(@PathVariable Id id) {
        final User user = this.queryBus.send(new RetrieveUserById((id)));
        return new UserDTO(user.getId(), user.getLastname(), user.getFirstname());
    }
    //endregion

    //region addMember
    @PostMapping(value = "/createUser", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Id add(@RequestBody CreateUserDTO userDTO) {
        return commandBus.send(new AddUser(userDTO.lastname, userDTO.firstname));
    }

    @PostMapping(value = "/updatePassword", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object updatePassword(@RequestBody UpdateUserPasswordDTO userDTO) {
        commandBus.send(new UpdateUserPassword(userDTO.id, userDTO.password));
        return null;
    }

    @PostMapping(value = "/updateCreditCard", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object updateCreditCard(@RequestBody UpdateUserCreditCardDTO userDTO) {
        commandBus.send(new UpdateUserCreditCard(userDTO.id, userDTO.creditCard));
        return null;
    }

    @PostMapping(value = "/updateUserCategory", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object updateUserCategory(@RequestBody UpdateUserCategoryDTO userDTO) {
        commandBus.send(new UpdateUserCategory(userDTO.id, userDTO.category));
        return null;
    }

    @PostMapping(value = "/setTradesmanJob", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object setTradesmanJob(@RequestBody SetTradesmanJobDTO userDTO) {
        final Job job = Job.values()[userDTO.job];
        commandBus.send(new SetTradesmanJob(userDTO.tradesmanId, job, new DailyRate(userDTO.dailyRate)));
        return null;
    }

    @PostMapping(value = "/approveUser", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public Object approveUser(@RequestBody Id id) {
        commandBus.send(new ApproveUser(id));
        return null;
    }
    //endregion
}
