package fr.al.cc1.use_cases.user.add_member.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.add_member.command.UpdateUserCreditCard;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class UpdateUserCreditCardCommandHandler implements CommandHandler<UpdateUserCreditCard, Void> {
    private final Repository<User> userRepository;

    public UpdateUserCreditCardCommandHandler(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Void handle(UpdateUserCreditCard command) {
        final User user = userRepository.findById(command.userId);
        user.changeCreditCard(command.creditCard);
        userRepository.save(user);
        return null;
    }
}
