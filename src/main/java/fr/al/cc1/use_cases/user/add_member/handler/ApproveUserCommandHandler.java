package fr.al.cc1.use_cases.user.add_member.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Price;
import fr.al.cc1.use_cases.user.add_member.event.UserAddedEvent;
import fr.al.cc1.use_cases.user.add_member.event.ValidUserApplicationEvent;
import fr.al.cc1.use_cases.user.add_member.command.ApproveUser;
import fr.al.cc1.use_cases.user.kernel.application.businessLayer.PaymentManager;
import fr.al.cc1.use_cases.user.kernel.domain.Contractor;
import fr.al.cc1.use_cases.user.kernel.domain.Tradesman;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import fr.al.cc1.use_cases.user.kernel.domain.exception.InvalidUserApplicationException;
import fr.al.cc1.use_cases.user.kernel.domain.exception.PaymentException;

public final class ApproveUserCommandHandler implements CommandHandler<ApproveUser, Void> {
    private final Repository<User> userRepository;
    private final EventDispatcher eventDispatcher;
    private final PaymentManager paymentManager;
    private static final Price REGISTRATION_FEE = new Price(10.0);

    public ApproveUserCommandHandler(Repository<User> userRepository, EventDispatcher eventDispatcher, PaymentManager paymentManager) {
        this.userRepository = userRepository;
        this.eventDispatcher = eventDispatcher;
        this.paymentManager = paymentManager;
    }

    @Override
    public Void handle(ApproveUser command) {
        final User user = userRepository.findById(command.userId);
        System.out.println("Adding new member " + user);
        //verify application
        if (!isApplicationValid(user)) {
            throw new InvalidUserApplicationException();
        }
        eventDispatcher.dispatch(new ValidUserApplicationEvent(user));
        //payment service
        if (!isPaymentSuccessful(user)) {
            throw new PaymentException();
        }
        //register
        user.validate();
        this.userRepository.save(user);
        eventDispatcher.dispatch(new UserAddedEvent(user));
        return null;
    }

    private boolean isApplicationValid(User user) {
        return user.getClass() != User.class
                && user.hasCreditCard()
                && !user.isValid()
                && (user.getClass() == Tradesman.class && ((Tradesman) user).getJob() != null || user.getClass() == Contractor.class);
    }

    private boolean isPaymentSuccessful(User user) {
        return paymentManager.processPayment(user, REGISTRATION_FEE);
    }

}
