package fr.al.cc1.use_cases.user.kernel.domain;

import fr.al.cc1.use_cases.kernel.domain.Price;

public interface PaymentAPI {
    boolean pay(CreditCard card, Price amount);
}
