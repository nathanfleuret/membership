package fr.al.cc1.use_cases.user.monthly_payment.task;

import fr.al.cc1.kernel.CommandBus;
import fr.al.cc1.kernel.QueryBus;
import fr.al.cc1.use_cases.user.kernel.application.RetrieveValidUsers;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import fr.al.cc1.use_cases.user.monthly_payment.application.ProcessRegularPayment;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;

public final class MonthlyPayment extends TimerTask {
    private final QueryBus queryBus;
    private final CommandBus commandBus;

    public MonthlyPayment(QueryBus queryBus, CommandBus commandBus) {
        this.queryBus = queryBus;
        this.commandBus = commandBus;
    }

    @Override
    public void run() {
        System.out.println(new Date() + " Launching monthly payment");

        List<User> validUsers = this.queryBus.send(new RetrieveValidUsers());
        for (User user : validUsers) {
            if (user.getLastPayment().plusMonths(1).isBefore(LocalDateTime.now())) {
                System.out.println(new Date() + " Processing regular payment for user " + user);
                this.commandBus.send(new ProcessRegularPayment(user.getId()));
            }
        }
        System.out.println(new Date() + " Monthly payment process finished");
    }
}
