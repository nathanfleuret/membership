package fr.al.cc1.use_cases.user.kernel.exposition;

import fr.al.cc1.kernel.domain.Id;

public final class UserDTO {
    public final Id id;
    public final String lastname;
    public final String firstname;

    public UserDTO(Id id, String lastname, String firstname) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                '}';
    }
}
