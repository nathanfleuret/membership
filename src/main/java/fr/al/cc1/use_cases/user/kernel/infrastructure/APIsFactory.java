package fr.al.cc1.use_cases.user.kernel.infrastructure;

import fr.al.cc1.use_cases.user.kernel.domain.PaymentAPI;

public final class APIsFactory {

    public PaymentAPI createPaymentAPI() {
        return new PaymentAPIStub();
    }
}
