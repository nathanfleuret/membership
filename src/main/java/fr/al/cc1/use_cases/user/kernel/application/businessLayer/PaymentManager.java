package fr.al.cc1.use_cases.user.kernel.application.businessLayer;

import fr.al.cc1.use_cases.user.kernel.domain.PaymentAPI;
import fr.al.cc1.use_cases.kernel.domain.Price;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public class PaymentManager {
private final PaymentAPI paymentAPI;

    public PaymentManager(PaymentAPI paymentAPI) {
        this.paymentAPI = paymentAPI;
    }

    public boolean processPayment(User user, Price amount) {
        return paymentAPI.pay(user.getCard(), amount);
    }
}
