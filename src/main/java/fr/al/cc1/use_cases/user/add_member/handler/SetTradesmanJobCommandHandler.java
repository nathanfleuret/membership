package fr.al.cc1.use_cases.user.add_member.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.add_member.command.SetTradesmanJob;
import fr.al.cc1.use_cases.user.kernel.domain.Tradesman;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class SetTradesmanJobCommandHandler implements CommandHandler<SetTradesmanJob,Void> {
    private final Repository<User> userRepository;

    public SetTradesmanJobCommandHandler(Repository<User> userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public Void handle(SetTradesmanJob command) {
        Tradesman tradesman = (Tradesman) userRepository.findById(command.tradesmanId);
        tradesman.setJob(command.job);
        tradesman.setDailyRate(command.dailyRate);
        userRepository.save(tradesman);
        return null;
    }
}
