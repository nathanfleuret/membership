package fr.al.cc1.use_cases.user.kernel.exposition;

import fr.al.cc1.kernel.domain.Id;

public final class UpdateUserPasswordDTO {
    public Id id;
    public String password;

    public UpdateUserPasswordDTO(Id id, String password) {
        this.id = id;
        this.password = password;
    }

    @Override
    public String toString() {
        return "UpdateUser{" +
                "id=" + id +
                ", password='" + password + '\'' +
                '}';
    }
}
