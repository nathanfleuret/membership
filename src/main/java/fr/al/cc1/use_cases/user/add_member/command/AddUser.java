package fr.al.cc1.use_cases.user.add_member.command;

import fr.al.cc1.kernel.domain.Command;

public final class AddUser implements Command {

    public final String lastname;
    public final String firstname;

    public AddUser(String lastname, String firstname){
        this.firstname = firstname;
        this.lastname = lastname;

    }

}
