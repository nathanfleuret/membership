package fr.al.cc1.use_cases.user.add_member.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Price;
import fr.al.cc1.use_cases.user.add_member.event.UserAddedEvent;
import fr.al.cc1.use_cases.user.add_member.command.ProceedUserApplicationPayment;
import fr.al.cc1.use_cases.user.kernel.application.businessLayer.PaymentManager;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import fr.al.cc1.use_cases.user.kernel.domain.exception.PaymentException;

public final class ProceedUserApplicationPaymentCommandHandler implements CommandHandler<ProceedUserApplicationPayment, Void> {
    private final EventDispatcher eventDispatcher;
    private final PaymentManager paymentManager;
    private final Repository<User> userRepository;
    private static final Price APPLICATION_FEE = new Price(10.0);

    public ProceedUserApplicationPaymentCommandHandler(EventDispatcher eventDispatcher, PaymentManager paymentManager, Repository<User> userRepository) {
        this.eventDispatcher = eventDispatcher;
        this.paymentManager = paymentManager;
        this.userRepository = userRepository;
    }

    @Override
    public Void handle(ProceedUserApplicationPayment command) {
        final User user = userRepository.findById(command.userId);
        if (!isPaymentSuccessful(user)) {
            throw new PaymentException();
        }
        eventDispatcher.dispatch(new UserAddedEvent(user));
        return null;
    }

    private boolean isPaymentSuccessful(User user) {
        return paymentManager.processPayment(user, APPLICATION_FEE);
    }
}
