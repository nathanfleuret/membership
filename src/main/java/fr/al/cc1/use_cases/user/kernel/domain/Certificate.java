package fr.al.cc1.use_cases.user.kernel.domain;

import java.util.Objects;

public final class Certificate {
    private final String title;

    public Certificate(String title) {
        this.title = Objects.requireNonNull(title);
    }

    @Override
    public String toString() {
        return "Diploma{" +
                "title= " + title
                + "}";
    }
}
