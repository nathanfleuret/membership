package fr.al.cc1.use_cases.user.kernel.application;

import fr.al.cc1.kernel.QueryHandler;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.kernel.domain.User;

import java.util.List;

public final class RetrieveUsersQueryHandler implements QueryHandler<RetrieveUsers, List<User>> {
    private final Repository<User> userRepository;

    public RetrieveUsersQueryHandler(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> handle(RetrieveUsers query) {
        return this.userRepository.findAll();
    }
}
