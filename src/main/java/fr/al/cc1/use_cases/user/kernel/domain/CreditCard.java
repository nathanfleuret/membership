package fr.al.cc1.use_cases.user.kernel.domain;

public final class CreditCard {
    private final String number;

    public CreditCard(String number) {
        if (!isValid(number)) {
            throw new IllegalArgumentException("The card number must respect the appropriate format.");
        }
        this.number = number;
    }

    private boolean isValid(String number) {
        String regex = "[0-9]+";
        return number.length() == 16 && number.matches(regex);
    }

    @Override
    public String toString() {
        return "CreditCard{" +
                "number= " + number
                + "}";
    }
}
