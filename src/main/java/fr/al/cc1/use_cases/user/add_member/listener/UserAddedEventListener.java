package fr.al.cc1.use_cases.user.add_member.listener;

import fr.al.cc1.kernel.EventListener;
import fr.al.cc1.use_cases.user.add_member.event.UserAddedEvent;

public final class UserAddedEventListener implements EventListener<UserAddedEvent> {

    @Override
    public void listenTo(UserAddedEvent event) {
        System.out.println("member added successfully : " + event.user);
    }
}
