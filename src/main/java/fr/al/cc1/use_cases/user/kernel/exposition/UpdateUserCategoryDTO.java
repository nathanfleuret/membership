package fr.al.cc1.use_cases.user.kernel.exposition;

import fr.al.cc1.kernel.domain.Id;

public final class UpdateUserCategoryDTO {
    public Id id;
    public String category;

    public UpdateUserCategoryDTO(Id id, String category) {
        this.id = id;
        this.category = category;
    }
}
