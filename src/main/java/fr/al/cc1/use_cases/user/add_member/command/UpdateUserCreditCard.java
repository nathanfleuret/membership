package fr.al.cc1.use_cases.user.add_member.command;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.user.kernel.domain.CreditCard;

public final class UpdateUserCreditCard implements Command {

    public final Id userId;
    public final CreditCard creditCard;

    public UpdateUserCreditCard(Id userId, CreditCard creditCard) {
        this.userId = userId;
        this.creditCard = creditCard;
    }

}
