package fr.al.cc1.use_cases.user.kernel.domain;

import fr.al.cc1.use_cases.kernel.domain.Address;
import fr.al.cc1.kernel.domain.Id;

public final class UserBuilder {
    private Id id;
    private String lastname;
    private String firstname;
    private String password;
    private Address address;
    private CreditCard creditCard;

    public UserBuilder withId(Id id) {
        this.id = id;
        return this;
    }

    public UserBuilder withLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public UserBuilder withFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public UserBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder withAddress(Address address) {
        this.address = address;
        return this;
    }

    public UserBuilder withCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
        return this;
    }

    public User build() {
        return User.of(id, lastname, firstname, password, address, creditCard);
    }
}
