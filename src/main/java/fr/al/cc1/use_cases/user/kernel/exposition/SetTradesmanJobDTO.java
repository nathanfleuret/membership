package fr.al.cc1.use_cases.user.kernel.exposition;

import fr.al.cc1.kernel.domain.Id;

public final class SetTradesmanJobDTO {
    public final Id tradesmanId;
    public final int job;
    public final double dailyRate;

    public SetTradesmanJobDTO(Id tradesmanId, int job, double dailyRate) {
        this.tradesmanId = tradesmanId;
        this.job = job;
        this.dailyRate = dailyRate;
    }
}
