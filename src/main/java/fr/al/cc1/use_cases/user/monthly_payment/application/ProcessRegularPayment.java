package fr.al.cc1.use_cases.user.monthly_payment.application;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;

public final class ProcessRegularPayment implements Command {
    public final Id userId;

    public ProcessRegularPayment(Id userId) {
        this.userId = userId;
    }
}
