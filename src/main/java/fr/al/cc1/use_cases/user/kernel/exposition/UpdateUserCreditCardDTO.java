package fr.al.cc1.use_cases.user.kernel.exposition;

import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.user.kernel.domain.CreditCard;

public final class UpdateUserCreditCardDTO {
    public Id id;
    public CreditCard creditCard;

    public UpdateUserCreditCardDTO(Id id, CreditCard creditCard) {
        this.id = id;
        this.creditCard = creditCard;
    }
}
