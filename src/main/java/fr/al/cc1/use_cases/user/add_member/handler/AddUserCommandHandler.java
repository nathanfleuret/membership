package fr.al.cc1.use_cases.user.add_member.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.add_member.command.AddUser;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import fr.al.cc1.use_cases.user.kernel.domain.UserBuilder;

public final class AddUserCommandHandler implements CommandHandler<AddUser, Id> {
    private final Repository<User> userRepository;

    public AddUserCommandHandler(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Id handle(AddUser command) {
        final User user = new UserBuilder()
                .withId(userRepository.nextIdentity())
                .withFirstname(command.firstname)
                .withLastname(command.lastname)
                .build();
        this.userRepository.save(user);
        return user.getId();
    }
}
