package fr.al.cc1.use_cases.user.kernel.infrastructure;

import fr.al.cc1.kernel.CommandBus;
import fr.al.cc1.kernel.QueryBus;
import fr.al.cc1.use_cases.user.monthly_payment.task.MonthlyPayment;

import java.util.Timer;

public final class TaskScheduler {

    public static final long ONE_DAY = 1000 * 60 * 60 * 24;
    private final CommandBus commandBus;
    private final QueryBus queryBus;

    public TaskScheduler(CommandBus commandBus, QueryBus queryBus) {
        this.commandBus = commandBus;
        this.queryBus = queryBus;
    }

    public void start() {
        Timer timer;
        timer = new Timer();
        //scheduler will launch MonthlyPayment task every 24h
        timer.schedule(new MonthlyPayment(queryBus, commandBus), 1000, ONE_DAY);
    }
}
