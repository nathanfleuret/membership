package fr.al.cc1.use_cases.user.add_member.event;

import fr.al.cc1.kernel.ApplicationEvent;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class UserAddedEvent implements ApplicationEvent {
    public final User user;

    public UserAddedEvent(User user) {
        this.user = user;
    }
}
