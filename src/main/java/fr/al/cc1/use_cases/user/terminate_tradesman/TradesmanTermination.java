package fr.al.cc1.use_cases.user.terminate_tradesman;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;

public final class TradesmanTermination implements Command {
    public Id tradesmanId;
}
