package fr.al.cc1.use_cases.user.kernel.domain;

import fr.al.cc1.use_cases.kernel.domain.Address;
import fr.al.cc1.kernel.domain.Id;

public final class Contractor extends User {
    private Contractor(Id userId, String lastname, String firstname, String password, Address address, CreditCard creditCard) {
        super(userId, lastname, firstname, password, address, creditCard);
    }

    public static Contractor of(User user) {
        return new Contractor(user.getId(), user.lastname, user.firstname, user.password, user.address, user.creditCard);
    }

    @Override
    public String toString() {
        return "Contractor{" +
                "userId=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", password='" + password + '\'' +
                ", creditCard='" + creditCard + '\'' +
                ", isValid='" + isValid + '\'' +
                ", lastPayment='" + lastPayment + '\'' +
                '}';
    }
}
