package fr.al.cc1.use_cases.user.kernel.application.proxy;

import fr.al.cc1.use_cases.user.kernel.application.businessLayer.AuthenticationManager;
import fr.al.cc1.use_cases.user.kernel.application.businessLayer.PaymentManager;
import fr.al.cc1.use_cases.user.kernel.domain.PaymentAPI;
import fr.al.cc1.use_cases.kernel.domain.Price;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class PaymentManagerProxy extends PaymentManager {
    private final AuthenticationManager authenticationManager;

    public PaymentManagerProxy(PaymentAPI paymentAPI, AuthenticationManager authenticationService) {
        super(paymentAPI);
        this.authenticationManager = authenticationService;
    }

    @Override
    public boolean processPayment(User user, Price amount) {
        try {
            authenticationManager.authenticate();
            return super.processPayment(user, amount);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }
}
