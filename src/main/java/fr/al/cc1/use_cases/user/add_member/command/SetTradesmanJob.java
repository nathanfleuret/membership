package fr.al.cc1.use_cases.user.add_member.command;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.kernel.domain.DailyRate;
import fr.al.cc1.use_cases.kernel.domain.Job;

public final class SetTradesmanJob implements Command {
    public final Id tradesmanId;
    public final Job job;
    public final DailyRate dailyRate;

    public SetTradesmanJob(Id tradesmanId, Job job, DailyRate dailyRate){
        this.tradesmanId = tradesmanId;
        this.job = job;
        this.dailyRate = dailyRate;
    }

}
