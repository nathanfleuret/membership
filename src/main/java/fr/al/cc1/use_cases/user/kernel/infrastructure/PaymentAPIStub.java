package fr.al.cc1.use_cases.user.kernel.infrastructure;

import fr.al.cc1.use_cases.user.kernel.domain.PaymentAPI;
import fr.al.cc1.use_cases.user.kernel.domain.CreditCard;
import fr.al.cc1.use_cases.kernel.domain.Price;

public final class PaymentAPIStub implements PaymentAPI {
    @Override
    public boolean pay(CreditCard card, Price amount) {
        System.out.println("Processing payment of " + amount);
        System.out.println("card: " + card);
        return true;
    }
}
