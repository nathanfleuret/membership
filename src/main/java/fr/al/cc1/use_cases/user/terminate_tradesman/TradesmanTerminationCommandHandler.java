package fr.al.cc1.use_cases.user.terminate_tradesman;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Intervention;
import fr.al.cc1.use_cases.kernel.domain.Project;
import fr.al.cc1.use_cases.user.kernel.domain.Tradesman;
import fr.al.cc1.use_cases.user.kernel.domain.User;

import java.util.List;
import java.util.stream.Collectors;

public final class TradesmanTerminationCommandHandler implements CommandHandler<TradesmanTermination, Void> {

    private final Repository<User> userRepository;
    private final Repository<Project> projectRepository;

    public TradesmanTerminationCommandHandler(Repository<User> userRepository, Repository<Project> projectRepository) {
        this.userRepository = userRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public Void handle(TradesmanTermination command) {
        final Tradesman tradesman = (Tradesman) userRepository.findById(command.tradesmanId);

        //project ids for this tradesman
        List<Id> projectIds = tradesman.getTradesmanInterventions()
                .stream()
                .map(Intervention::getProjectId)
                .collect(Collectors.toList());

        final List<Project> activeProjects = projectRepository
                .find(project -> projectIds.contains(project.getId()))
                .stream().filter(Project::isActive)
                .collect(Collectors.toList());

        for (Project project : activeProjects) {
            //remove tradesman from active projects
            project.getAssignedTradesmen().remove(tradesman);
            //remove project ids from tradesman interventions
            tradesman.getTradesmanInterventions()
                    .removeIf(intervention -> intervention.getProjectId() == project.getId());

            projectRepository.save(project);
        }
        userRepository.save(tradesman);
        return null;
    }
}
