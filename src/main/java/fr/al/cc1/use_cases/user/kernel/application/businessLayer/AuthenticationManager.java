package fr.al.cc1.use_cases.user.kernel.application.businessLayer;

import fr.al.cc1.use_cases.user.kernel.domain.exception.AuthenticationException;

public final class AuthenticationManager {
    public void authenticate() throws Exception {
        System.out.println("authenticating...");
        if (false) {
            throw new AuthenticationException();
        }
    }
}
