package fr.al.cc1.use_cases.user.add_member.command;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;

public final class UpdateUserCategory implements Command {
    public final Id userId;
    public final String category;


    public UpdateUserCategory(Id userId, String category) {
        this.userId = userId;
        this.category = category;
    }
}
