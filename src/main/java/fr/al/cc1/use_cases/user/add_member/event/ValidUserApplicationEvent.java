package fr.al.cc1.use_cases.user.add_member.event;

import fr.al.cc1.kernel.ApplicationEvent;
import fr.al.cc1.kernel.Event;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class ValidUserApplicationEvent implements ApplicationEvent {
    public final User user;

    public ValidUserApplicationEvent(User user) {
        this.user = user;
    }
}
