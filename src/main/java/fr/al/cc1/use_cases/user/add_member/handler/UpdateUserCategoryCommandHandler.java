package fr.al.cc1.use_cases.user.add_member.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.add_member.command.UpdateUserCategory;
import fr.al.cc1.use_cases.user.kernel.domain.Contractor;
import fr.al.cc1.use_cases.user.kernel.domain.Tradesman;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class UpdateUserCategoryCommandHandler implements CommandHandler<UpdateUserCategory, Void> {
    private final Repository<User> userRepository;

    public UpdateUserCategoryCommandHandler(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Void handle(UpdateUserCategory command) {
        final User user = userRepository.findById(command.userId);
        switch (command.category) {
            case "contractor":
                userRepository.save(Contractor.of(user));
                break;
            case "tradesman":
                Tradesman tradesman = Tradesman.of(user);
                //tradesman.changeDiploma("RIL");
                userRepository.save(tradesman);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + command.category);
        }
        return null;
    }
}
