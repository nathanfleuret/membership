package fr.al.cc1.use_cases.user.kernel.application.proxy;

import fr.al.cc1.use_cases.user.kernel.application.businessLayer.AuthenticationManager;
import fr.al.cc1.use_cases.user.kernel.application.businessLayer.PaymentManager;
import fr.al.cc1.use_cases.user.kernel.domain.PaymentAPI;

public final class PaymentManagerProxyFactory {
    private final AuthenticationManager authenticationManager;

    public PaymentManagerProxyFactory(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    public PaymentManager withPaymentAPI(PaymentAPI paymentAPI) {
        return new PaymentManagerProxy(paymentAPI, authenticationManager);
    }
}
