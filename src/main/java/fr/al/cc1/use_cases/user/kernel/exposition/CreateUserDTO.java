package fr.al.cc1.use_cases.user.kernel.exposition;

public final class CreateUserDTO {
    public String lastname;
    public String firstname;

    public CreateUserDTO() {}
    public CreateUserDTO(String lastname, String firstname) {
        this.lastname = lastname;
        this.firstname = firstname;
    }

    @Override
    public String toString() {
        return "CreateUser{" +
                "lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                '}';
    }
}
