package fr.al.cc1.use_cases.user.kernel.domain;

import fr.al.cc1.use_cases.kernel.domain.*;
import fr.al.cc1.kernel.domain.Id;

import java.util.ArrayList;
import java.util.List;

public final class Tradesman extends User {

    private List<Certificate> certificates;
    private List<Intervention> interventions;
    private Job job;
    private DailyRate dailyRate;

    private Tradesman(Id id, String lastname, String firstname, String password, Address address, CreditCard creditCard) {
        super(id, lastname, firstname, password, address, creditCard);
        this.interventions = new ArrayList<>();
    }

    public static Tradesman of(User user) {
        return new Tradesman(user.getId(), user.lastname, user.firstname, user.password, user.address, user.creditCard);
    }

    public void addDiploma(String title) {
        Certificate certificate = new Certificate(title);
        if (certificates == null)
            certificates = new ArrayList<>();
        this.certificates.add(certificate);
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Job getJob() {
        return this.job;
    }

    public DailyRate getDailyRate() {
        return this.dailyRate;
    }

    public void setDailyRate(DailyRate dailyRate) {
        this.dailyRate = dailyRate;
    }

    public List<Intervention> getTradesmanInterventions(){return this.interventions;}

    public boolean isAvailableFor(WindowedPeriod period) {
        for (Intervention intervention : interventions) {
            if (intervention.overlapsWith(period))
                return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Tradesman{" +
                "userId=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", password='" + password + '\'' +
                ", creditCard='" + creditCard + '\'' +
                ", certificates='" + certificates + '\'' +
                ", job='" + job + '\'' +
                ", dailyRate='" + dailyRate + '\'' +
                ", isValid='" + isValid + '\'' +
                ", lastPayment='" + lastPayment + '\'' +
                '}';
    }
}



