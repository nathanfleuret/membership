package fr.al.cc1.use_cases.user.monthly_payment.application;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Price;
import fr.al.cc1.use_cases.user.kernel.application.businessLayer.PaymentManager;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class ProcessRegularPaymentCommandHandler implements CommandHandler<ProcessRegularPayment, Void> {
    private final Repository<User> userRepository;
    private final PaymentManager paymentManager;
    private static final Price REGULAR_FEE = new Price(5.0);

    public ProcessRegularPaymentCommandHandler(Repository<User> userRepository, PaymentManager paymentManager) {
        this.userRepository = userRepository;
        this.paymentManager = paymentManager;
    }

    @Override
    public Void handle(ProcessRegularPayment command) {
        final User user = this.userRepository.findById(command.userId);
        this.paymentManager.processPayment(user, REGULAR_FEE);
        user.updateLastPayment();
        this.userRepository.save(user);
        return null;
    }
}
