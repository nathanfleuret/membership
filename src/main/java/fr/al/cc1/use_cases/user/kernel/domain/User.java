package fr.al.cc1.use_cases.user.kernel.domain;

import fr.al.cc1.use_cases.kernel.domain.Address;
import fr.al.cc1.kernel.domain.Entity;
import fr.al.cc1.kernel.domain.Id;

import java.time.LocalDateTime;
import java.util.Objects;

public class User extends Entity {

    protected final String lastname;
    protected final String firstname;
    protected Address address;
    protected String password;
    protected CreditCard creditCard;
    protected boolean isValid;
    protected LocalDateTime lastPayment;

    protected User(Id id, String lastname, String firstname, String password, Address address, CreditCard creditCard) {
        this.id = id;
        this.lastname = Objects.requireNonNull(lastname);
        this.firstname = Objects.requireNonNull(firstname);
        this.password = password;
        this.address = address;
        this.creditCard = creditCard;
    }

    public static User of(Id userId, String lastname, String firstname, String password, Address address, CreditCard creditCard) {
        return new User(userId, lastname, firstname, password, address, creditCard);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return isValid == user.isValid && lastname.equals(user.lastname) && firstname.equals(user.firstname) && address.equals(user.address) && password.equals(user.password) && creditCard.equals(user.creditCard) && lastPayment.equals(user.lastPayment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastname, firstname, address, password, creditCard, isValid, lastPayment);
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public boolean isValid() {
        return isValid;
    }

    public LocalDateTime getLastPayment() {
        return lastPayment;
    }

    public void changePassword(String newPassword) {
        this.password = Objects.requireNonNull(newPassword);
    }

    public void changeCreditCard(CreditCard creditCard) {
        this.creditCard = Objects.requireNonNull(creditCard);
    }

    public void validate() {
        this.isValid = true;
        this.lastPayment = LocalDateTime.now();
    }

    public void updateLastPayment() {
        this.lastPayment = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                ", password='" + password + '\'' +
                ", creditCard='" + creditCard + '\'' +
                '}';
    }

    public CreditCard getCard() {
        return creditCard;
    }

    public boolean hasCreditCard() {
        return creditCard != null;
    }


}