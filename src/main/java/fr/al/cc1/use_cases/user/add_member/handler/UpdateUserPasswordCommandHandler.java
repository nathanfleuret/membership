package fr.al.cc1.use_cases.user.add_member.handler;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.add_member.command.UpdateUserPassword;
import fr.al.cc1.use_cases.user.kernel.domain.User;

public final class UpdateUserPasswordCommandHandler implements CommandHandler<UpdateUserPassword, Void> {
    private final Repository<User> userRepository;

    public UpdateUserPasswordCommandHandler(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Void handle(UpdateUserPassword command) {
        final User user = userRepository.findById(command.userId);
        user.changePassword(command.password);
        userRepository.save(user);
        return null;
    }
}
