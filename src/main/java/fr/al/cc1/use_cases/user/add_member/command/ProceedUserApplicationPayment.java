package fr.al.cc1.use_cases.user.add_member.command;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;

public final class ProceedUserApplicationPayment implements Command {
    public final Id userId;

    public ProceedUserApplicationPayment(Id userId) {
        this.userId = userId;
    }
}
