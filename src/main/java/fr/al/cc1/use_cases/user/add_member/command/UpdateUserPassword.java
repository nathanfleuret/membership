package fr.al.cc1.use_cases.user.add_member.command;

import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;

public final class UpdateUserPassword implements Command {
    public final Id userId;
    public final String password;

    public UpdateUserPassword(Id userId, String password) {
        this.userId = userId;
        this.password = password;
    }

}
