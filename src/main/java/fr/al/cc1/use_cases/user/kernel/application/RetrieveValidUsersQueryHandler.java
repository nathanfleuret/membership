package fr.al.cc1.use_cases.user.kernel.application;

import fr.al.cc1.kernel.QueryHandler;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.kernel.domain.User;

import java.util.List;

public final class RetrieveValidUsersQueryHandler implements QueryHandler<RetrieveValidUsers, List<User>> {
    private final Repository<User> userRepository;

    public RetrieveValidUsersQueryHandler(Repository<User> userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> handle(RetrieveValidUsers query) {
        return this.userRepository.find(User::isValid);
    }
}
