package fr.al.cc1.use_cases.kernel.domain;

public final class ProjectNeed {
    private final Job job;
    private DailyRate dailyRate;
    private int tradesmanQuantity;


    public ProjectNeed(Job job, DailyRate dailyRate, int tradesmanQuantity){
        checkPrecondition(tradesmanQuantity);
        this.job = job;
        this.dailyRate = dailyRate;
        this.tradesmanQuantity = tradesmanQuantity;
    }

    public void checkPrecondition(int tradesmanQuantity){
        if(tradesmanQuantity<1) throw new IllegalArgumentException("value cannot be less than 1 ");
    }

    public Job getJob(){return this.job;}
    public DailyRate getDailyRate(){return this.dailyRate;}
    public int getTradesmanQuantity(){return this.tradesmanQuantity;}

    @Override
    public String toString() {
        return "{" + "Project needs for the job"+job.toString()
                +"with a daily payment of"+dailyRate.toString()+
                "for a number of "+tradesmanQuantity+"applicants for the project"+"}"
                ;
    }
}