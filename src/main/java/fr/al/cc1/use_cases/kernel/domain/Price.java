package fr.al.cc1.use_cases.kernel.domain;

public final class Price implements Comparable<Price>{
    private final Double value;
    private static final char currency = '€';

    public Price(Double value) {
        checkPreconditions(value);
        this.value = value;
    }

    private void checkPreconditions(Double value) {
        if (value < 0) {
            throw new IllegalArgumentException("value cannot be negative");
        }
    }

    @Override
    public String toString() {
        return String.valueOf(value) + currency;
    }

    @Override
    public int compareTo(Price other) {
        return this.value.compareTo(other.value);
    }
}
