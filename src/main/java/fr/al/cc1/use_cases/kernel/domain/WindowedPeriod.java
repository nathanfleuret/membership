package fr.al.cc1.use_cases.kernel.domain;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

public final class WindowedPeriod {
    private final LocalDateTime startDate;
    private final LocalDateTime endDate;

    public WindowedPeriod(LocalDateTime startDate, LocalDateTime endDate) {
        checkPreconditions(startDate, endDate);
        this.startDate = startDate;
        this.endDate = endDate;
    }

    private void checkPreconditions(LocalDateTime startDate, LocalDateTime endDate) {
        if (endDate.isBefore(startDate)) {
            throw new IllegalArgumentException("start date must be before end date.");
        }
    }

    public boolean overlapsWith(WindowedPeriod other) {
        return (this.endDate.isBefore(other.startDate) || other.endDate.isBefore(this.startDate));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WindowedPeriod that = (WindowedPeriod) o;
        return startDate == that.startDate && endDate == that.endDate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDate, endDate);
    }
}
