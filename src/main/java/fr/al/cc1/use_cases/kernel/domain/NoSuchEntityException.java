package fr.al.cc1.use_cases.kernel.domain;

import fr.al.cc1.kernel.domain.Id;

public final class NoSuchEntityException extends RuntimeException {
    public NoSuchEntityException(String message) {
        super(message);
    }

    public static NoSuchEntityException withId(Id id) {
        return new NoSuchEntityException(String.format("No entity found with ID %d.", id.getValue()));
    }
}
