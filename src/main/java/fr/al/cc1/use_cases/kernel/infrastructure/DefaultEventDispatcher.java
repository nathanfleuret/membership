package fr.al.cc1.use_cases.kernel.infrastructure;

import fr.al.cc1.kernel.Event;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.kernel.EventListener;

import java.util.List;
import java.util.Map;

public final class DefaultEventDispatcher<E extends Event> implements EventDispatcher<E> {

    private final Map<Class<E>, List<EventListener<E>>> eventListenersMap;

    public DefaultEventDispatcher(Map<Class<E>, List<EventListener<E>>> eventListenersMap) {
        this.eventListenersMap = eventListenersMap;
    }

    @Override
    public void dispatch(E event) {
        final List<EventListener<E>> eventListeners = eventListenersMap.get(event.getClass());
        if (eventListeners != null) {
            eventListeners.forEach(eventListener -> eventListener.listenTo(event));
        }
    }
}
