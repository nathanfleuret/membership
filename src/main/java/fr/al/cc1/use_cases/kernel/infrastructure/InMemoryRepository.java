package fr.al.cc1.use_cases.kernel.infrastructure;

import fr.al.cc1.use_cases.kernel.domain.NoSuchEntityException;
import fr.al.cc1.kernel.domain.Entity;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Repository;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class InMemoryRepository<T extends Entity> implements Repository<T> {
    private final AtomicInteger counter = new AtomicInteger(0);
    private final Map<Id, T> data = new ConcurrentHashMap<>();

    @Override
    public List<T> findAll() {
        return (List<T>) List.copyOf(data.values());
    }

    @Override
    public List<T> find(Predicate<T> predicate) {
        return List.copyOf(data.values())
                .stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public T findById(Id id) throws NoSuchEntityException {
        final T entity = data.get(id);
        if (entity == null) {
            throw NoSuchEntityException.withId(id);
        }
        return entity;
    }

    @Override
    public Id save(T entity) {
        data.put(entity.getId(), entity);
        return entity.getId();
    }

    @Override
    public Id nextIdentity() {
        return Id.of(counter.incrementAndGet());
    }

    @Override
    public void delete(T entity) {
        data.remove(entity.getId());
    }

    @Override
    public void delete(List<T> entitiesToDelete) {
        for (T entity : entitiesToDelete) {
            data.remove(entity.getId());
        }
    }

    @Override
    public void deleteById(Id id) {
        data.remove(id);
    }
}
