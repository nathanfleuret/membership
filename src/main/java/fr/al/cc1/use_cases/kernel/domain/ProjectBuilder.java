package fr.al.cc1.use_cases.kernel.domain;

import fr.al.cc1.kernel.domain.Id;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public final class ProjectBuilder {

    private Id id;
    private Id contractorId;
    private String title;
    private WindowedPeriod period;
    private Address location;
    private List<ProjectNeed> needs;

    public ProjectBuilder withId(Id id) {
        this.id = id;
        return this;
    }

    public ProjectBuilder withContractorId(Id contractorId) {
        this.contractorId = contractorId;
        return this;
    }

    public ProjectBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    public ProjectBuilder withWindowedPeriod(LocalDateTime startDate, LocalDateTime endDate) {
        this.period = new WindowedPeriod(startDate, endDate);
        return this;
    }

    public ProjectBuilder withAddress(Address location) {
        this.location = location;
        return this;
    }

    public ProjectBuilder withNeeds(List<ProjectNeed> needs) {
        this.needs = needs;
        return this;
    }

    public Project build() {
        return Project.of(id, contractorId, title, period, location);
    }
}
