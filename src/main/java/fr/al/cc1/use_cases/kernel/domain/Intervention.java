package fr.al.cc1.use_cases.kernel.domain;

import fr.al.cc1.kernel.domain.Entity;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.user.kernel.domain.Tradesman;

public final class Intervention extends Entity {

    private final WindowedPeriod period;
    private final Id projectId;

    public Intervention(WindowedPeriod period, Id projectId) {
        this.period = period;
        this.projectId = projectId;
    }

    public Id getProjectId() {
        return this.projectId;
    }

    public WindowedPeriod getPeriod() {
        return this.period;
    }

    public boolean overlapsWith(WindowedPeriod otherPeriod) {
        return this.period.overlapsWith(otherPeriod);
    }
}
