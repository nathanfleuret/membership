package fr.al.cc1.use_cases.kernel.domain;

public final class DailyRate implements Comparable<DailyRate>{

    private final Price price;
    private static final String frequency = "/day";

    public DailyRate(Double value){
        this.price = new Price(value);
    }

    public String toString(){
        return price.toString() + frequency;
    }

    @Override
    public int compareTo(DailyRate other) {
        return this.price.compareTo(other.price);
    }
}
