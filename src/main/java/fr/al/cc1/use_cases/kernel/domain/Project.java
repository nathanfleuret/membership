package fr.al.cc1.use_cases.kernel.domain;

import fr.al.cc1.kernel.domain.Entity;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.user.kernel.domain.Tradesman;

import java.util.ArrayList;
import java.util.List;

public final class Project extends Entity {

    private final Id contractorId;
    private final String title;
    private WindowedPeriod period;
    private final Address location;
    private List<ProjectNeed> needs;
    private final List<Tradesman> assignedTradesmen;
    private Boolean isActive = false;

    private Project(Id id, Id contractorId, String title, WindowedPeriod period, Address location) {
        this.contractorId = contractorId;
        this.id = id;
        this.title = title;
        this.location = location;
        this.period = period;
        this.assignedTradesmen = new ArrayList<>();
    }

    public static Project of(Id id, Id contractorId, String title, WindowedPeriod period, Address location) {
        return new Project(id, contractorId, title, period, location);
    }

    public void setNeeds(List<ProjectNeed> needs) {
        if (needs != null && !needs.isEmpty())
            this.needs = needs;
    }

    public List<ProjectNeed> getNeeds() {
        return this.needs;
    }

    public Id getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setPeriod(WindowedPeriod period) {
        this.period = period;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void activate() {
        if (this.needs != null && !this.needs.isEmpty() && this.period != null)
            this.isActive = true;
        else
            throw new InvalidProjectActivationException();
    }

    public void deactivate() {
        this.isActive = false;
    }

    public WindowedPeriod getPeriod() {
        return this.period;
    }

    public List<Tradesman> getAssignedTradesmen() {
        return this.assignedTradesmen;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                "contractorId=" + contractorId +
                ", title='" + title + '\'' +
                ", location='" + location + '\'' +
                ", period='" + period + '\'' +
                ", needs='" + needs + '\'' +
                ", isActive='" + isActive + '\'' +
                '}';
    }
}
