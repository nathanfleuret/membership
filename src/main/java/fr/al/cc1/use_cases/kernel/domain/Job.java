package fr.al.cc1.use_cases.kernel.domain;

public enum Job {

    ELECTRICIAN("Electrician"),
    PLUMBER("Plumber");

    public final String description;

    Job(String description) {
        this.description = description;
    }

}
