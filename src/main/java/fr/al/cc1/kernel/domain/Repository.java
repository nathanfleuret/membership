package fr.al.cc1.kernel.domain;

import fr.al.cc1.use_cases.kernel.domain.NoSuchEntityException;

import java.util.List;
import java.util.function.Predicate;

public interface Repository<T extends Entity> {
    List<T> findAll();

    List<T> find(Predicate<T> predicate);

    T findById(Id id) throws NoSuchEntityException;

    Id save(T entity);

    Id nextIdentity();

    void delete(T entity);

    void delete(List<T> entitiesToDelete);

    void deleteById(Id id);
}
