package fr.al.cc1.kernel.domain;

import fr.al.cc1.kernel.domain.Id;

public abstract class Entity {
    protected Id id;

    public Id getId() {
        return id;
    }
}
