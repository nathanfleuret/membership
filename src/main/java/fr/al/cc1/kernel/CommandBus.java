package fr.al.cc1.kernel;

import fr.al.cc1.kernel.domain.Command;

public interface CommandBus {
    <C extends Command, R> R send(C command);
}