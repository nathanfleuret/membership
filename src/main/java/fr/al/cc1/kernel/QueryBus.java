package fr.al.cc1.kernel;

import fr.al.cc1.kernel.domain.Query;

public interface QueryBus {
    <Q extends Query, R> R send(Q query);
}
