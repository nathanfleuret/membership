package fr.al.cc1.kernel;

import fr.al.cc1.kernel.domain.Query;

public interface QueryHandler<Q extends Query, R> {
    R handle(Q query);
}
