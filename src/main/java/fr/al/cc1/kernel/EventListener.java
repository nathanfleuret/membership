package fr.al.cc1.kernel;

public interface EventListener<E extends Event> {
    void listenTo(E event);
}
