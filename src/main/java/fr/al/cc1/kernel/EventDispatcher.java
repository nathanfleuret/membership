package fr.al.cc1.kernel;

public interface EventDispatcher<E extends Event> {
    void dispatch(E event);
}
