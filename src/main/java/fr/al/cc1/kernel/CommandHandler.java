package fr.al.cc1.kernel;

import fr.al.cc1.kernel.domain.Command;

public interface CommandHandler<C extends Command, R> {
    R handle(C command);
}
