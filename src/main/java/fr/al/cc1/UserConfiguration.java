package fr.al.cc1;

import fr.al.cc1.kernel.*;
import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Query;
import fr.al.cc1.use_cases.kernel.domain.Intervention;
import fr.al.cc1.use_cases.kernel.domain.Project;
import fr.al.cc1.use_cases.kernel.infrastructure.DefaultEventDispatcher;
import fr.al.cc1.use_cases.project.close_project.*;
import fr.al.cc1.use_cases.project.close_project.command.CloseProject;
import fr.al.cc1.use_cases.project.close_project.handler.CloseProjectCommandHandler;
import fr.al.cc1.use_cases.project.create_project.*;
import fr.al.cc1.use_cases.project.create_project.command.ActivateProject;
import fr.al.cc1.use_cases.project.create_project.command.CreateProject;
import fr.al.cc1.use_cases.project.create_project.ProjectCreatedEvent;
import fr.al.cc1.use_cases.project.create_project.command.UpdateProject;
import fr.al.cc1.use_cases.project.create_project.handler.ActivateProjectCommandHandler;
import fr.al.cc1.use_cases.project.create_project.handler.CreateProjectCommandHandler;
import fr.al.cc1.use_cases.project.create_project.handler.UpdateProjectCommandHandler;
import fr.al.cc1.use_cases.user.add_member.command.*;
import fr.al.cc1.use_cases.user.add_member.event.UserAddedEvent;
import fr.al.cc1.use_cases.user.add_member.event.ValidUserApplicationEvent;
import fr.al.cc1.use_cases.user.add_member.handler.*;
import fr.al.cc1.use_cases.user.add_member.listener.UserAddedEventListener;
import fr.al.cc1.use_cases.user.add_member.listener.ValidUserApplicationEventListener;
import fr.al.cc1.use_cases.user.kernel.application.*;
import fr.al.cc1.use_cases.user.kernel.application.businessLayer.AuthenticationManager;
import fr.al.cc1.use_cases.user.kernel.application.businessLayer.PaymentManager;
import fr.al.cc1.use_cases.user.kernel.application.proxy.PaymentManagerProxyFactory;
import fr.al.cc1.use_cases.user.kernel.domain.Contractor;
import fr.al.cc1.use_cases.user.kernel.domain.PaymentAPI;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import fr.al.cc1.use_cases.user.kernel.infrastructure.TaskScheduler;
import fr.al.cc1.use_cases.user.kernel.infrastructure.PaymentAPIStub;
import fr.al.cc1.use_cases.kernel.infrastructure.InMemoryRepository;
import fr.al.cc1.use_cases.project.request_tradesman.application.RequestTradesman;
import fr.al.cc1.use_cases.project.request_tradesman.application.RequestTradesmanCommandHandler;
import fr.al.cc1.use_cases.user.monthly_payment.application.ProcessRegularPayment;
import fr.al.cc1.use_cases.user.monthly_payment.application.ProcessRegularPaymentCommandHandler;
import fr.al.cc1.use_cases.user.terminate_tradesman.TradesmanTermination;
import fr.al.cc1.use_cases.user.terminate_tradesman.TradesmanTerminationCommandHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class UserConfiguration {

    @Bean
    public Repository<User> userRepository() {
        return new InMemoryRepository<>();
    }

    @Bean
    public Repository<Project> projectRepository() {
        return new InMemoryRepository<>();
    }

    @Bean
    public Repository<Contractor> contractorRepository() {
        return new InMemoryRepository<>();
    }

    @Bean
    public Repository<Intervention> interventionRepository() {
        return new InMemoryRepository<>();
    }

    @Bean
    public AuthenticationManager authenticationManager() {
        return new AuthenticationManager();
    }

    @Bean
    public PaymentManagerProxyFactory paymentManagerProxyFactory() {
        return new PaymentManagerProxyFactory(new AuthenticationManager());
    }

    @Bean
    public PaymentManager paymentManager() {
        return paymentManagerProxyFactory().withPaymentAPI(paymentAPI());
    }

    @Bean
    public PaymentAPI paymentAPI() {
        return new PaymentAPIStub();
    }

    @Bean
    public CommandBus commandBus() {
        final Map<Class<? extends Command>, CommandHandler> commandHandlerMap = new HashMap<>();
        //add member
        commandHandlerMap.put(AddUser.class, new AddUserCommandHandler(userRepository()));
        commandHandlerMap.put(ApproveUser.class, new ApproveUserCommandHandler(userRepository(), eventDispatcher(), paymentManager()));
        commandHandlerMap.put(ProceedUserApplicationPayment.class, new ProceedUserApplicationPaymentCommandHandler(eventDispatcher(), paymentManager(), userRepository()));
        commandHandlerMap.put(UpdateUserCategory.class, new UpdateUserCategoryCommandHandler(userRepository()));
        commandHandlerMap.put(UpdateUserCreditCard.class, new UpdateUserCreditCardCommandHandler(userRepository()));
        commandHandlerMap.put(UpdateUserPassword.class, new UpdateUserPasswordCommandHandler(userRepository()));
        commandHandlerMap.put(SetTradesmanJob.class, new SetTradesmanJobCommandHandler(userRepository()));

        //match tradesman
        commandHandlerMap.put(RequestTradesman.class, new RequestTradesmanCommandHandler(userRepository(), projectRepository()));

        //monthly payment
        commandHandlerMap.put(ProcessRegularPayment.class, new ProcessRegularPaymentCommandHandler(userRepository(), paymentManager()));

        //terminate tradesman
        commandHandlerMap.put(TradesmanTermination.class, new TradesmanTerminationCommandHandler(userRepository(), projectRepository()));

        // create project use case
        commandHandlerMap.put(CreateProject.class, new CreateProjectCommandHandler(eventDispatcher(), projectRepository(), userRepository()));
        commandHandlerMap.put(ActivateProject.class, new ActivateProjectCommandHandler(eventDispatcher(), projectRepository()));
        commandHandlerMap.put(UpdateProject.class, new UpdateProjectCommandHandler(eventDispatcher(), projectRepository()));
        //close project
        commandHandlerMap.put(CloseProject.class, new CloseProjectCommandHandler(eventDispatcher(), projectRepository(), userRepository()));

        return new SimpleCommandBus(commandHandlerMap);
    }

    @Bean
    public QueryBus queryBus() {
        final HashMap<Class<? extends Query>, QueryHandler> queryHandlerMap = new HashMap<>();
        queryHandlerMap.put(RetrieveUsers.class, new RetrieveUsersQueryHandler(userRepository()));
        queryHandlerMap.put(RetrieveUserById.class, new RetrieveUserByIdQueryHandler(userRepository()));
        queryHandlerMap.put(RetrieveValidUsers.class, new RetrieveValidUsersQueryHandler(userRepository()));
        return new SimpleQueryBus(queryHandlerMap);
    }

    @Bean
    public EventDispatcher<Event> eventDispatcher() {
        final Map<Class<? extends Event>, List<EventListener<? extends Event>>> listenerMap = new HashMap<>();
        listenerMap.put(UserAddedEvent.class, List.of(new UserAddedEventListener()));
        listenerMap.put(ValidUserApplicationEvent.class, List.of(new ValidUserApplicationEventListener()));
        listenerMap.put(ProjectCreatedEvent.class, List.of(new ProjectCreatedEventListener()));
        listenerMap.put(ProjectClosedEvent.class, List.of(new ProjectClosedEventListener()));
        return new DefaultEventDispatcher(listenerMap);
    }

    @Bean
    public TaskScheduler taskScheduler() {
        return new TaskScheduler(commandBus(), queryBus());
    }
}
