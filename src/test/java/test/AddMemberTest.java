package test;

import fr.al.cc1.kernel.CommandBus;
import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.SimpleCommandBus;
import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.infrastructure.InMemoryRepository;
import fr.al.cc1.use_cases.user.add_member.command.AddUser;
import fr.al.cc1.use_cases.user.add_member.handler.AddUserCommandHandler;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

public class AddMemberTest {
private Repository<User> userRepository;
private Map<Class<? extends Command>, CommandHandler> commandHandlerMap;

    @Before
    public void initialise() {
        userRepository = new InMemoryRepository<>();
        commandHandlerMap = new HashMap<>();
    }

    @Test
    public void addUser(){
        commandHandlerMap.put(AddUser.class, new AddUserCommandHandler(userRepository));
        CommandBus commandBus = new SimpleCommandBus(commandHandlerMap);

        AddUser command = new AddUser("fleuret", "nathan");
        Id userId = commandBus.send(command);

        User user = userRepository.findById(userId);
        assertEquals(command.lastname, user.getLastname());
        assertEquals(command.firstname, user.getFirstname());
    }

}
