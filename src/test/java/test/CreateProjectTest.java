package test;

import fr.al.cc1.kernel.CommandHandler;
import fr.al.cc1.kernel.Event;
import fr.al.cc1.kernel.EventDispatcher;
import fr.al.cc1.kernel.EventListener;
import fr.al.cc1.kernel.domain.Command;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.domain.Project;
import fr.al.cc1.use_cases.kernel.infrastructure.DefaultEventDispatcher;
import fr.al.cc1.use_cases.kernel.infrastructure.InMemoryRepository;
import fr.al.cc1.use_cases.project.create_project.command.CreateProject;
import fr.al.cc1.use_cases.project.create_project.handler.CreateProjectCommandHandler;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class CreateProjectTest {
    @Test
    public void createProjectTest() {
        Repository<User> userRepository = new InMemoryRepository<>();
        Repository<Project> projectRepository = new InMemoryRepository<Project>();
        final Map<Class<? extends Event>, List<EventListener<? extends Event>>> listenerMap = new HashMap<>();
        EventDispatcher<Event> eventDispatcher = new DefaultEventDispatcher(listenerMap);
        Map<Class<? extends Command>, CommandHandler> commandHandlerMap = new HashMap<>();


        commandHandlerMap.put(CreateProject.class, new CreateProjectCommandHandler(eventDispatcher, projectRepository, userRepository));
    }

}
