package test;

import fr.al.cc1.use_cases.kernel.domain.Price;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PriceTest {
    @Test
    void priceWillThrow() {
        Assertions.assertThrows( IllegalArgumentException.class, this::priceNotOK);
    }

    @Test
    void priceWillNotThrow() {
        Assertions.assertDoesNotThrow(this::priceOK);
    }

    private void priceNotOK() {
        Price price = new Price(-1.0);
    }

    private void priceOK() {
        Price price = new Price(3.0);
    }
}
