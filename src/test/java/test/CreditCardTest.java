package test;

import fr.al.cc1.use_cases.user.kernel.domain.CreditCard;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CreditCardTest {

    @Test
    void cardTooShort() {
        Assertions.assertThrows( IllegalArgumentException.class, this::cardNumberTooShort);
    }

    @Test
    void cardContainsLetters() {
        Assertions.assertThrows( IllegalArgumentException.class, this::cardNumberWithLetters);
    }

    @Test
    void cardWillNotThrow() {
        Assertions.assertDoesNotThrow(this::cardOK);
    }

    private void cardNumberTooShort() {
        CreditCard card = new CreditCard("123");
    }

    private void cardNumberWithLetters() {
        CreditCard card = new CreditCard("123456789123456a");
    }

    private void cardOK() {
        CreditCard card = new CreditCard("1234567812345678");
    }

}
