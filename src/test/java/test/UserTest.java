package test;

import fr.al.cc1.kernel.domain.Repository;
import fr.al.cc1.use_cases.kernel.infrastructure.InMemoryRepository;
import fr.al.cc1.use_cases.user.add_member.command.AddUser;
import fr.al.cc1.use_cases.user.add_member.handler.AddUserCommandHandler;
import fr.al.cc1.use_cases.user.kernel.domain.UserBuilder;
import fr.al.cc1.use_cases.kernel.domain.Address;
import fr.al.cc1.kernel.domain.Id;
import fr.al.cc1.use_cases.user.kernel.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserTest {
    @Test
    void userWillNotThrow() {
        Assertions.assertDoesNotThrow(this::userOK);
    }

    @Test
    void userBuilderWillNotThrow() {
        Assertions.assertDoesNotThrow(this::userBuilderOK);
    }



    private void userWithoutPassword() {
        User user = User.of(Id.of(1), "Lastname", "Firstname", null, new Address("Rouen"), null);
    }

    private void userOK() {
        User user = User.of(Id.of(1), "Lastname", "Firstname", "password", new Address("Rouen"), null);
    }

    private void userBuilderOK() {
        User user = new UserBuilder()
                .withId(Id.of(1))
                .withLastname("Lastname")
                .withFirstname("Firstname")
                .withPassword("password")
                .build();
    }
}
